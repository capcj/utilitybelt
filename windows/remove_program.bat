::Removes Any Program with wmic
::Call remove_program.bat ProgramName
@echo off
setlocal EnableDelayedExpansion
set program=%~1%
shift
set wmic_output=0
for /f %%f in ('wmic /interactive:off product where "Name like '%%%program%%%'"') do (
        IF %%f EQU 1 (
         set wmic_output=1
         break
        )
) 
IF %wmic_output% NEQ 1 (
    echo "Nao esta registrado no wmic!"
    for /f %%f in ('FINDSTR /S /M /C:"Unin" /D:"%PROGRAMFILES(x86)%/%program%"^;"%PROGRAMFILES%/%program%" *.exe') do (
        set _f=%%f
        set extension=!_f:~-3!
        set program_name=!_f:.exe=!
        IF "!extension!" EQU "exe" (
            IF "!program_name!" EQU "Uninstall" (
                echo "%%f - Desinstalador encontrado!"
                IF EXIST "%PROGRAMFILES(x86)%/%program%/%%f" (
                    "%PROGRAMFILES(x86)%/%program%/%%f" /s
                    echo "Desinstalacao executada"
                ) ELSE (
                    "%PROGRAMFILES%/%program%/%%f" /s
                    echo "Desinstalacao executada"
                )
            )
        )
    ) 
) ELSE (
    wmic /interactive:off product where "Name like '%%%program%%%'" call uninstall /nointeractive
    echo "%program% Desinstalado"
)
