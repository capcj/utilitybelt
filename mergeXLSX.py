#!/usr/bin/env python3

#USAGE
#pip install pandas xlrd
#mergeXLSX.py <dir> output.xlsx 
#where output.xlsx is the unified file
#or
#mergeXLSX.py <dir>
#the last one will put a file called mergedfile.xlsx in the current mergeXLSX.py script dir
#
#This works FROM/TO the xlsx format. Libreoffice might help to convert from xls.
#
#CREDITS TO DSM (https://stackoverflow.com/users/487339/dsm) (almost the entire program)
#
#

import sys
import pandas as pd
from os import listdir
from os.path import isfile, join

excel_names = [sys.argv[1] + '\\' + f for f in listdir(sys.argv[1]) if isfile(join(sys.argv[1], f)) and f.find('xlsx') != -1]
excels = [pd.ExcelFile(name) for name in excel_names]
frames = [x.parse(x.sheet_names[0], header=None,index_col=None) for x in excels]

# delete the first row for all frames except the first
# i.e. remove the header row -- assumes it's the first
frames[1:] = [df[1:] for df in frames[1:]]

combined = pd.concat(frames)

# write it out
newfile = sys.argv[2] if len(sys.argv) > 2 else 'mergedfile.xlsx'
combined.to_excel(newfile, header=False, index=False)