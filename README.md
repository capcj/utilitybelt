# utilitybelt

A idea to put useful stuff that I use sometimes

## Tools
- mergeXLSX.py (Python): Merges a bunch of XLSX files into one;

### Windows Tools
- remove_program.bat (Batch Script) - Removes a program using wmic
or searching for Uninstall.exe in the %PROGRAMFILES% (x86, x86_64)
due the auto-extractors hell;
